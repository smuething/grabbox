#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name = 'grabbox',
    version = '0.1.0',
    description = 'The next level utility grabbag! (Actually, grabbag was already taken)',
    author = 'Steffen Müthing',
    author_email = 'steffen.muething@iwr.uni-heidelberg.de',
    license = 'BSD',
    keywords = 'utility',
    packages = find_packages(),
    install_requires = [
        ],
    entry_points = {
        },
)
