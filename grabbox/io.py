

class InputChomper:

    def __init__(self,separator=b'\n',data=b'',encoding='utf-8'):
        self.data = bytearray(data)
        self.separator = separator
        self.encoding = encoding

    def append(self,data):
        self.data.extend(data)

    def read_chunks(self,data=b''):
        self.append(data)
        chunks = self.data.split(self.separator)
        encoding = self.encoding
        if encoding is not None:
            for chunk in chunks[:-1]:
                yield chunk.decode(encoding=encoding)
        else:
            for chunk in chunks[:-1]:
                yield chunk
        if len(chunks) > 1:
            self.data[:] = chunks[-1]

    def peek(self):
        if self.encoding is not None:
            return self.data.decode(encoding=self.encoding)
        else:
            return bytes(self.data)

    def read(self,n=-1,data=b''):
        self.data.extend(data)
        if n == 0:
            return '' if self.encoding is not None else b''
        elif n == -1:
            if self.encoding is not None:
                out = self.data.decode(encoding=self.encoding)
            else:
                out = bytes(self.data)
            self.data = bytearray()
            return out
        else:
            out = self.data[:n]
            if self.encoding is not None:
                out = out.decode(encoding=self.encoding)
            self.data = self.data[n:]
            return out
