import click
import logbook
import lya

from .util import ContextManagerContainer
from ..logbook import LogLevel
from logbook import StderrHandler, NullHandler
from logbook.handlers import DEFAULT_FORMAT_STRING
from ..namedtuple import DefaultableNamedTuple
from click import UsageError


class ColoredFormatter:

    class ColorInfo(DefaultableNamedTuple):

        def __init__(self, fg=None, bg=None, bold=None, dim=None, underline=None, reverse=None):
            pass

    COLORS = {
        LogLevel.DEBUG: ColorInfo(fg='cyan', dim=True),
        LogLevel.INFO: ColorInfo(fg='green',dim=True),
        LogLevel.NOTICE: ColorInfo(fg='cyan'),
        LogLevel.WARNING: ColorInfo(fg='yellow'),
        LogLevel.ERROR: ColorInfo(fg='magenta'),
        LogLevel.CRITICAL: ColorInfo(fg='red'),
    }

    def __init__(self, colorize=None, formatstring=None,stacktrace=False):
        self.colorize = colorize if colorize is not None else click.get_text_stream('stderr').isatty()
        self.stacktrace = stacktrace
        self.formatstring = formatstring
        if not formatstring:
            if click.get_text_stream('stderr').isatty():
                self.formatstring = '{record.channel}: {record.message}'
            else:
                self.formatstring = DEFAULT_FORMAT_STRING

    def style(self,msg,**kwargs):
        if self.colorize:
            return click.style(msg,**kwargs)
        else:
            return msg

    def __call__(self, record, handler):
        colors = self.COLORS[LogLevel(record.level)]
        msg = self.style(self.formatstring.format(record=record, handler=handler), **colors._asdict())
        if self.stacktrace and record.formatted_exception:
            msg = '\n'.join((msg, self.style(record.formatted_exception, **self.COLORS[LogLevel.ERROR]._asdict())))
        return msg


class LogHandler(StderrHandler):

    def __init__(self, level=LogLevel.NOTICE.value, colorize=None, formatstring=None, stacktrace=False):
        super().__init__(level)
        self.formatter = ColoredFormatter(colorize=colorize,formatstring=formatstring,stacktrace=stacktrace)


def init_logbook(logger, handler=LogHandler(), default_level=LogLevel.NOTICE, catch_exceptions=None):

    def decorator(f):

        def debug_callback(ctx, param, value):
            log_config = ctx.meta.setdefault('logbook.config', lya.AttrDict())
            if log_config.setdefault('initialized', False):
                if 'debug' in log_config and log_config.debug and not value:
                    raise UsageError(
                        'Cannot turn off debug mode again after activating')
                if value and 'level' in log_config:
                    raise UsageError(
                        'Cannot combine --debug-mode and verbosity flags')
            log_config.debug = value
            if value:
                manager = ContextManagerContainer.get(ctx)

                null_handler = NullHandler()
                manager.enter_context(null_handler.applicationbound, args=0)

                handler.level = LogLevel.DEBUG.value
                try:
                    handler.formatter.stacktrace = True
                except AttributeError:
                    pass

                manager.enter_context(handler.applicationbound, args=0)
                if catch_exceptions is not None:
                    manager.enter_context(catch_exceptions)
                    manager.add(catch_exceptions, front=True)
                manager.add(handler.applicationbound, front=True, args=0)
                manager.add(null_handler.applicationbound, front=True, args=0)
                logger.notice('Running in debug mode')
                log_config.handler = handler
                log_config.initialized = True
            return value

        def callback(ctx, param, value):
            log_config = ctx.meta.setdefault('logbook.config', lya.AttrDict())
            debug = log_config.get('debug', False)
            if log_config.setdefault('initialized', False):
                if debug and value:
                    raise UsageError(
                        'Cannot combine --debug-mode and verbosity flags')
            else:
                log_config.setdefault('level', default_level)
                log_config.level = True

            log_config.setdefault('verbose', 0)
            log_config.setdefault('quiet', 0)
            log_config[param.name] = value

            if log_config.initialized:
                if not debug:
                    # just update the verbosity
                    level = LogLevel.clamp(
                        default_level - log_config.verbose + log_config.quiet)
                    log_config.handler.level = level
                    logger.level = level
            else:
                manager = ContextManagerContainer.get(ctx)

                null_handler = logbook.NullHandler()
                manager.enter_context(null_handler.applicationbound, args=0)

                handler.level = level = LogLevel.clamp(
                    default_level - log_config.verbose + log_config.quiet)

                logger.level = level

                manager.enter_context(handler.applicationbound, args=0)
                if catch_exceptions is not None:
                    manager.enter_context(catch_exceptions)
                    manager.add(catch_exceptions, front=True)
                manager.add(handler.applicationbound, front=True, args=0)
                manager.add(null_handler.applicationbound, front=True, args=0)
                logger.debug(
                    'Logging setup complete, log level = {}', level.name)
                log_config.handler = handler
                log_config.initialized = True

        f = click.option(
            '--debug/--no-debug',
            help='Turn on debug mode (detailed status information). Cannot be combined with -v and -q.',
            default=False,
            expose_value=False,
            callback=debug_callback,
            is_eager=True,
        )(f)
        f = click.option(
            '--quiet', '-q',
            help='Decrease amount of status messages. Repeating this option further increases verbosity.',
            count=True,
            default=0,
            expose_value=False,
            is_eager=True,
            callback=callback,
        )(f)
        f = click.option(
            '--verbose', '-v',
            help='Increase amount of status messages. Repeating this option further decreases verbosity.',
            count=True,
            default=0,
            expose_value=False,
            is_eager=True,
            callback=callback,
        )(f)
        return f

    return decorator
