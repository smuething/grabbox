from ..dependencies import ensure_min_python_version, ensure_setuptools_requirement

ensure_min_python_version(major=3,minor=3,name=__name__)
ensure_setuptools_requirement('click >= 5.0',name=__name__)

import click, click.core
from contextlib import contextmanager, ExitStack
from collections import deque
from functools import update_wrapper
from inspect import signature

class ContextManagerContainer:

    DICT_KEY = 'vault.ContextManagerContainer'

    @classmethod
    def install(cls,orig):
        cls.wrapped_context_manager = staticmethod(orig)
        return cls

    def __new__(cls,ctx,param=None):
        container = cls.get(ctx)
        if container.stack:
            return ExitStack()
        else:
            return container.enter(ctx,param)

    def __init__(self):
        self.context_managers = deque((self.wrapped_context_manager,))
        self.stack = None

    @classmethod
    def get(cls,ctx=None):
        ctx = ctx or click.get_current_context()
        meta = ctx.meta
        try:
            return meta[cls.DICT_KEY]
        except KeyError:
            ctx.meta[cls.DICT_KEY] = r = super().__new__(cls)
            r.__init__()
            return r

    @property
    def active(self):
        return self.stack is not None

    @contextmanager
    def activate(self,stack,*args):
        try:
            assert(self.stack is None)
            self.stack = stack
            self.cm_args = args
            yield
        finally:
            self.stack = None
            del self.cm_args

    def make_cm(self,context_manager,args=None):
        if args is None:
            sig = signature(context_manager)
            args = len(sig.parameters)
        if args == 2:
            return context_manager
        elif args == 1:
            return lambda ctx,param: context_manager(ctx)
        elif args == 0:
            return lambda ctx,param: context_manager()
        else:
            raise ValueError('context manager factory {} has invalid signature'.format(context_manager))


    def add(self,context_manager,front=False,args=None,activate=None):
        cm = self.make_cm(context_manager,args)
        if front:
            self.context_managers.appendleft(cm)
        else:
            self.context_managers.append(cm)
        if self.active:
            activate_now = activate if activate is not None else not front
            if activate_now:
                self.stack.enter_context(cm(*self.cm_args))

    def enter_context(self,context_manager,args=None):
        f = self.make_cm(context_manager,args)
        cm = f(*self.cm_args)
        self.stack.enter_context(cm)


    @contextmanager
    def enter(self,ctx,param):
        with ExitStack() as stack:
            stack.enter_context(self.activate(stack,ctx,param))
            context_managers = [ stack.enter_context(cm(ctx,param)) for cm in self.context_managers]
            yield

click.core.augment_usage_errors = ContextManagerContainer.install(click.core.augment_usage_errors)


def make_pass_decorator(object_type, ensure=False):
    """
    This is a copy of the original function from click. That function
    has a bug that causes it to swallow the first argument in the passed
    positional args.

    Given an object type this creates a decorator that will work
    similar to :func:`pass_obj` but instead of passing the object of the
    current context, it will find the innermost context of type
    :func:`object_type`.
    This generates a decorator that works roughly like this::
        from functools import update_wrapper
        def decorator(f):
            @pass_context
            def new_func(ctx, *args, **kwargs):
                obj = ctx.find_object(object_type)
                return ctx.invoke(f, obj, *args, **kwargs)
            return update_wrapper(new_func, f)
        return decorator
    :param object_type: the type of the object to pass.
    :param ensure: if set to `True`, a new object will be created and
                   remembered on the context if it's not there yet.
    """
    def decorator(f):
        def new_func(*args, **kwargs):
            ctx = click.get_current_context()
            if ensure:
                obj = ctx.ensure_object(object_type)
            else:
                obj = ctx.find_object(object_type)
            if obj is None:
                raise RuntimeError('Managed to invoke callback without a '
                                   'context object of type %r existing'
                                   % object_type.__name__)
            return ctx.invoke(f, obj, *args, **kwargs)
        return update_wrapper(new_func, f)
    return decorator


def inject_context(f):

    def inject(*args,**kwargs):
        ctx = click.get_current_context()
        with click.Context(ctx.command,parent=ctx) as new_ctx:
            return new_ctx.invoke(f,*args,**kwargs)

    return update_wrapper(inject,f)



VLINE = '│'
HLINE = '─'
TEE = '├'
LL_ANGLE = '└'

CONT_INDENT = TEE + 2*HLINE + ' '
CONT_NESTED_INDENT = VLINE + 3*' '
END_INDENT = LL_ANGLE + 2*HLINE + ' '
BLANK_INDENT = 4*' '

def fix_indent(indent,indent_level,level):
    if indent is BLANK_INDENT or indent is END_INDENT:
        return indent
    if indent_level == level:
        return indent
    else:
        return CONT_NESTED_INDENT

def prefix_yielder(n,level):
    i = 0
    while i < n:
        cur_level = yield CONT_INDENT
        if level == cur_level:
            i = i + 1
    yield END_INDENT
    while True:
        yield BLANK_INDENT

def _tree(root,level,prefix,formatter,recurse):
    if prefix is None:
        prefix = deque()
    c = prefix_yielder(len(root),level)
    next(c)
    prefix.append(c)
    for k,v in sorted(root.items()):
        p = ''.join(fix_indent(c.send(level),l,level) for l,c in enumerate(prefix))
        yield formatter(p,k,v,level)
        r = recurse(k,v,level)
        if r:
            yield from _tree(r,level+1,prefix,formatter,recurse)
    prefix.pop()

def tree(root,formatter=lambda p,k,v,l:p+k,recurse=lambda k,v,l: v if isinstance(v,dict) else None):
    return _tree(root,0,deque(),formatter,recurse)
