from .dependencies import ensure_min_python_version, ensure_package
from .exceptions import UnsupportedPythonVersion

# We require Python 3 for the new-style metaclass declaration
ensure_min_python_version(3,name=__name__)

# Get function signature handling from somehwere
try:
    ensure_min_python_version(3,3,name=__name__)
    from inspect import signature, Parameter
except UnsupportedPythonVersion:
    ensure_package('funcsigs',name=__name__)
    from funcsigs import signature, Parameter

from collections import namedtuple

class DefaultableNamedTupleType(type):
    """The metaclass that does the heavy lifting
    """

    def __new__(cls, name, bases, namespace, **kwds):
        # Don't do anything for the marker base class
        if name == 'DefaultableNamedTuple':
            return type.__new__(cls,name,bases,namespace)

        # grab the constructor from the original definition and then kill it
        init = namespace['__init__']
        del namespace['__init__']

        # create namedtuple base class from constructor signature
        sig = signature(init)
        tuple_base = namedtuple(name,list(sig.parameters.keys())[1:])
        bases = tuple_base,

        # extract signature of namedtuple's __new__() and update it with the
        # (possibly defaulted) parameters from the original constructor
        new_sig = signature(tuple_base.__new__)
        params = [next(iter(new_sig.parameters.values()))]
        params.extend(list(sig.parameters.values())[1:])
        new_sig = new_sig.replace(parameters=params)

        # create wrapper for __new__()
        def new_with_defaults(cls,*args,**kwargs):
            ba = new_sig.bind(cls,*args,**kwargs)

            # apply defaults
            for param in new_sig.parameters.values():
                if param.name not in ba.arguments and param.default is not param.empty:
                    ba.arguments[param.name] = param.default

            # use enhanced arguments from bound arguments for actual call
            return tuple_base.__new__(*ba.args,**ba.kwargs)

        # insert our improved __new__
        new_with_defaults.__name__ = '__new__'
        new_with_defaults.__doc__ = init.__doc__ or 'Create new instance of ColorInfo{}'.format(new_sig.replace(parameters=params[1:]))
        new_with_defaults.__module = init.__module__
        namespace['__new__'] = new_with_defaults

        # make sure we don't create an instance dict
        namespace['__slots__'] = ()

        return type.__new__(cls,name,bases,namespace)

class DefaultableNamedTuple(metaclass=DefaultableNamedTupleType):
    """Base class for namedtuple types with default values.

    The default values are read from the constructor of the derived class.
    Important: This constructor will not be present on the actual type!
    """

    __slots__ = ()
