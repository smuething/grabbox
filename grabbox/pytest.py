import pytest
from itertools import zip_longest

def assert_generator(generator,*args):
    __tracebackhide__ = True

    nargs = len(args)
    i = -1 # make sure we have i if generator is empty

    for i,(value,expected) in enumerate(zip_longest(generator,args)):
        assert i < nargs, 'generator too long, only expected {} value(s)'.format(nargs)
        assert value == expected

    assert i + 1 == nargs, 'generator too short, expected {} values, only got {}'.format(nargs,i+1)
